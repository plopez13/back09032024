const express = require('express');
const app = express();
const methodOverride = require('method-override');
const bodyParser = require('body-parser');
const expressLayout = require('express-layouts');


// Configurar Express para usar EJS como motor de plantillas
app.set('view engine', 'ejs');
app.set('layout', 'layout')
app.use(expressLayout); // Usa express-layouts

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Middleware para method-override
app.use(methodOverride('_method'));

// Middleware para analizar el cuerpo de la solicitud
//app.use(express.json());
//app.use(express.urlencoded({ extended: true }));

// Controladores de las rutas
require("./controllers/comidasController")(app);
require("./controllers/categoriasController")(app);
require("./controllers/clientesController")(app);

app.get("/", (req,res) => {
    res.render('home');
})

app.listen(3005, function () {
    console.log("Servidor iniciado en el puerto 3005");
});



