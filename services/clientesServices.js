
const conn = require('../config/conn')

const clientes = {
    async postClientes(nombre, email, direccion, telefono) {
        try {
            //const sql = 'INSERT INTO clientes (nombre, email, direccion, telefono) VALUES (?,?,?,?)'  // falla, hay que pasar id=null
            const sql = `INSERT INTO clientes (id, nombre, email, direccion, telefono) VALUES (${null},"${nombre}","${email}","${direccion}","${telefono}")`
            const resultado = await conn.query(sql, [nombre,email,direccion,telefono])
            console.log(resultado)
            if (resultado.affectedRows > 0) {
                return {mensaje: 'Cliente agregado correctamente con el id ' + resultado.insertId}
            } else {
                throw new Error('No se pudo agregar al cliente (desde service)');
            }
        } catch (error) {
            console.error('Error en la consulta SQL (service)', error);
            throw new Error('Error en la consulta SQL (service');
        }

    },

    async getClientes(req,res) {
        try {
            const sql = 'SELECT * FROM clientes'
            const resultado = await conn.query(sql)

            if (resultado.length > 0) {
                return resultado
            } else {
                throw new Error('No se encontró ningún cliente');
            }
        } catch (error) {
            console.error('Error en la consulta SQL (service)', error);
            throw new Error('Error en la consulta SQL (service');
        }

    },

    async deleteClientes(id) {
        try {
            //const {id} = req.body;
            const sql = `DELETE FROM clientes WHERE id = ${id}`    // no funciona con = ?
            const resultado = await conn.query(sql, [id])

            if (resultado.affectedRows > 0) {
                return resultado
            } else {
                throw new Error('No se pudo eliminar el cliente ' + id);
            }
        } catch (error) {
            console.error('Error en la consulta de eliminación SQL (service)', error);
            throw new Error('Error en la consulta de eliminación SQL (service');
        }

    }




}

module.exports = clientes
