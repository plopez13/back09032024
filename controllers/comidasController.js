//creamos el módulo a exportar
//Al ser llamado en index.js recibe las capacidades de express, para ser utilizado
module.exports = function (app) {
    app.get("/ensalada", async function (req, res) {
                //requerimos y guardamos la ruta de services donde hara la consulta a la base
                const comidas = require("./../services/comidasServices")
                //hacemos la consulta y ponemos await porque no sabemos cuanto puede demorar
                //podemos seguir viendo el proceso en  clientesServices.
                const response = await comidas.getComidas()
        res.send(JSON.stringify(response))
    })
    app.get("/asado", async function (req, res) {
        res.send("ahora si que si")
    })
}
