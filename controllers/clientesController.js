const clientesService = require('../services/clientesServices')

module.exports = function (app) {

    app.post("/clientes/bajaCli/:id", async function (req, res) {
        try {
            const { id } = req.params;
            const resultado = await clientesService.deleteClientes(id);
            res.json(resultado);
        } catch (error) {
            console.error('Error al eliminar el cliente (controller):', error);
            //res.status(500).json({error: 'Error al agregar el cliente.'})
        }
    });

    app.get("/clientes/alta", async function (req, res) {
        res.render('cliente_alta')
    });

    app.get("/clientes/baja", async function (req, res) {
        let id = 0
        res.render('cliente_baja', { id: id})
    });

    app.post("/clientes/alta", async function (req, res) {
        try {
            console.log(req.body)
            const { nombre, email, direccion, telefono } = req.body;
            const resultado = await clientesService.postClientes(nombre, email, direccion, telefono);
            res.json(resultado);
        } catch (error) {
            console.error('Error al agregar el cliente (controller):', error);
            //res.status(500).json({error: 'Error al agregar el cliente.'})
        }
    });



 
 
    app.get("/clientes", async function (req, res) {
        const comidas = require("./../services/clientesServices")
        const response = await comidas.getClientes()
        res.send(JSON.stringify(response))
    })
}