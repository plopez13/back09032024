//creamos el módulo a exportar
//Al ser llamado en index.js recibe las capacidades de express, para ser utilizado
module.exports = function (app) {
    app.get("/categorias", async function (req, res) {
                //requerimos y guardamos la ruta de services donde hara la consulta a la base
                const categorias = require("./../services/categoriasServices")
                //hacemos la consulta y ponemos await porque no sabemos cuanto puede demorar
                //podemos seguir viendo el proceso en  clientesServices.
                const response = await categorias.getCategorias()
        res.send(JSON.stringify(response))
    })
    app.get("/categorias/:id", async function (req, res) {
        let id = req.params.id
        //requerimos y guardamos la ruta de services donde hara la consulta a la base
        const categorias = require("./../services/categoriasServices")
        //hacemos la consulta y ponemos await porque no sabemos cuanto puede demorar
        //podemos seguir viendo el proceso en  clientesServices.
        const response = await categorias.getCategoriasById(id)
res.send(JSON.stringify(response))
})

}
